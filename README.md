# ETAPA DE SELEÇÃO: DESENVOLVIMENTO FRONT-END (ESTÁGIO) #
## DATA DE ENTREGA: 07/12/2017  ##

Desenvolver uma aplicação web utilizando AngularJS para cadastramento e consulta de eventos de um estabelecimento.

* Cadastrar evento.

* Apresentar uma tabela com todos os eventos cadastrados.

* Buscar evento por filtros (crie pelo menos 2 filtros).


Foi desenvolvido um projeto base para nortear o desenvolvimento da atividade. Esse projeto está disponível no seguinte repositório: https://luizbezerra@bitbucket.org/construtordigital/selecaocdl

Para implementação não é necessário uso de banco de dados, faça uso das variáveis do browser (local storage). 
Os descritivos dos eventos são id, título, data, horário de início, horário de término, nome do responsável, telefone do responsável.

### DICAS: ###

* Fazer uso do framework AngularJS Material para auxiliar nos componentes de interface. 

* Fazer uso de datapicker.


### ESCLARECIMENTOS: ###
Pastas:

* app_modulos: Contém os módulos desse projeto, onde "app" é a base para o projeto e as demais pastas são os módulos. No caso, contém apenas o módulo "eventos".

* gulpTasks: O projeto faz uso da lib Gulp para automatizar as tarefas do front-end.

* node_modules: Essa pasta será criada assim que for executado "npm install", ou seja, contém todas as bibliotecas/dependencias instaladas do projeto.

* public: Será criada ao executar o comando "npm run dev". O Gulp irá criar a pasta e adicionar os arquivos necessários para rodar o sistema no brownser.


### PASSO-A-PASSO: ###

1 Instalar o Git e NodeJS 

  * Git: https://git-scm.com/download
  
  * NodeJS: https://nodejs.org/en/
  
2 Pelo console (cmd), criar uma pasta de nome: selecaocdl

3 Baixando o projeto do repositório git.

  * git clone https://luizbezerra@bitbucket.org/construtordigital/selecaocdl.git
  
4 Instalando as dependencias (ou bibliotecas)

  * npm install
 
CASO NÃO CONSIGA EXECUTAR npm install, EXECUTE OS 2 COMANDOS ABAIXO:

  * npm install angular angular-animate admin-lte angular-toastr angular-ui-bootstrap babel-preset-es2015 font-awesome gulp gulp-angular-templatecache gulp-babel gulp-concat gulp-htmlmin gulp-load-plugins gulp-uglify gulp-uglifycss gulp-util gulp-webserver inputmask jquery jshint nodemon @uirouter/angularjs babel-core@6 gulp-watch --save-dev

  * npm install admin-lte angular-aria angular-messages angular-material --save

5 Para rodar o código.

  * npm run dev

Obs.: Foi instalado uma dependencia (gulp-watch) que a cada alteração nos arquivos html, css e javascript, o sistema é reiniciado. Evitando que seja necessário atualizar (F5) a página a cada alteração de código.