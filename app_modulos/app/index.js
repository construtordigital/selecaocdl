(function () {
   var manager = angular.module('manager', [
      'ui.router', //responsável pelas rotas
      'ui.bootstrap',
      'ngAnimate', //responsável pelas animações
      'ngMaterial',
      'toastr' //responsável pelas mensagens
   ])
})();