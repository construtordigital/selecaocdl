//declarando um componente chamado field
(function () {
   angular.module('manager').component('fieldButton', {
      bindings: { //definição dos parâmetros do componente
         id: '@', //string que não altera
         label: '@',
         icon: '@',
         button: '@',
         text: '@',
         idmodal: '@',
         char: '@',
         grid: '@',
         placeholder: '@',
         type: '@',
         mask: '@',
         pattern: '@',
         dir: '@',
         model: '=', //binding de duas direções onde as alterações do component reflete no controller e vice-versa
         click: '&',
         readonly: '<', //binding unidirecional ou seja a alteração feita no componente nao vai refletir no parent
         hide: '<'
      },
      controller: [ //controller para inserir comportamentos dentro do componente
         'gridSystem', //presente dentro do gridSystemFactory.js
         function (gridSystem) { //injeção de dependência
            //recebendo como parametro a proria grid declarada acima e atribuindo a
            //uma nova variavel criada dentro do this chamda gridClasses
            this.$onInit = () => this.gridClasses = gridSystem.toCssClasses(this.grid);
            //a linha acima será executado somente após a inicialização dos binding
         }
      ],
      template: //template que será alimentado pelo componente através da declaração
         //double mustache seguida da referência padrão do componente $ctrl acrescido
         //de variaveis e parametros declarados acima
         `
		 <div class="{{ $ctrl.gridClasses }}">
            <div class="form-group">
                <label for="{{ $ctrl.id }}">{{ $ctrl.label }}</label>
                <div class="input-group">
	                <input id="{{ $ctrl.id }}" class="form-control" placeholder="{{ $ctrl.placeholder }}"
	                type="{{ $ctrl.type }}" ng-model="$ctrl.model" ng-readonly="$ctrl.readonly" ng-hide="$ctrl.hide" ng-change="$ctrl.change" 
	                ui-mask="{{ $ctrl.mask }}" ng-pattern="{{ $ctrl.pattern }}" dir={{ $ctrl.dir }}/>
                    <span class="input-group-btn"> 
                    	<button class="btn {{ $ctrl.button }}" data-toggle="modal" data-target="#{{$ctrl.idmodal}}" 
                        ng-click="$ctrl.click()" >{{ $ctrl.text }} <span class="{{ $ctrl.icon }}"></span></button>
                    </span>                    
                </div>
            </div>
        </div>
        `
   });
})();