//declarando um componente chamado valueBox
(function () {
   angular.module('manager').component('infoBox', {
      bindings: { //definição dos parâmetros do componente
         grid: '@', //string que não se altera
         colorClass: '@',
         value: '@', //binding @ suporta variaveis como passagem de parametros
         text: '@',
         subtext: '@',
         iconClass: '@',
      },
      controller: [ //controller para inserir comportamentos dentro do componente
         'gridSystem', //presente dentro do gridSystemFactory.js
         function (gridSystem) { //injeção de dependência
            //recebendo como parametro a proria grid declarada acima e atribuindo a
            //uma nova variavel criada dentro do this chamada gridClasses
            this.$onInit = () => this.gridClasses = gridSystem.toCssClasses(this.grid);
            //a linha acima será executado somente após a inicialização dos binding
         }
      ],
      template: //template que será alimentado pelo componente através da declaração
         //double mustache seguida da referência padrão do componente $ctrl acrescido
         //de variaveis e parametros declarados acima
         `
        <div class="{{ $ctrl.gridClasses }}">
          <div class="small-box {{ $ctrl.colorClass }}">
            <div class="inner">
              <p no-padding no-margin>{{ $ctrl.text}}</p>
              <p no-padding no-margin>{{ $ctrl.subtext}}</p>
              <h2> {{ $ctrl.value }}</h2>
            </div>
            <div class="icon">
              <i class="fa {{ $ctrl.iconClass }}"></i>
            </div>
          </div>
        </div>
        `
   });
}());