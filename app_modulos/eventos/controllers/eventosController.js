(function () {
   angular.module('manager').controller('EventosCtrl', [
      '$stateParams',
      '$location',
      '$timeout',
      '$window',
      'msgs',
      EventosCtrl //referência da função declarada abaix
   ]);

   function EventosCtrl($stateParams, $location, $timeout, $window, msgs) {
      const vm = this;

      vm.eventos = [{
         id: 1,
         titulo: 'Evento A',
         data: new Date(2017, 10, 5),
         hora_inicio: new Date(2017, 10, 5, 10, 0, 0, 0),
         hora_termino: new Date(2017, 10, 5, 12, 0, 0, 0),
         nome_responsavel: 'Joao',
         telefone_responsavel: '(99) 99999-9999'
      }, {
         id: 2,
         titulo: 'Evento B',
         data: new Date(2017, 10, 6),
         hora_inicio: new Date(2017, 10, 5, 9, 0, 0, 0),
         hora_termino: new Date(2017, 10, 5, 10, 30, 0, 0),
         nome_responsavel: 'Maria',
         telefone_responsavel: '(99) 99999-9999'
      }];
      console.log(vm.eventos)

      vm.cadastrarEvento = function () {

      };

      vm.getEventos = function () {

      };

   }
})();