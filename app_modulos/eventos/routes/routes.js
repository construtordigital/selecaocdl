(function () {
   angular.module('manager').config([
      '$stateProvider', //presente dentro do ui-router servir as navegaçoes
      '$urlRouterProvider', //presente dentro do ui-router
      function ($stateProvider, $urlRouterProvider) { //injeção de dependência do angular
         $stateProvider.state('meusEventos', { //criar os estados da aplicação
            url: "^/meusEventos",
            templateUrl: "eventos/views/meusEventos.html",

         }).state('cadastrarEventos', { //criar os estados da aplicação
            url: "^/cadastrarEventos",
            templateUrl: "eventos/views/cadastrarEventos.html"
         });
         $urlRouterProvider.otherwise('/meusEventos'); //estado padrão caso não encontre os estados
      }
   ]);
})();