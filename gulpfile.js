//importando o modulo gulp responsável pelo build (fazedor de tarefas)
const gulp = require('gulp');

//importando o modulo gulp-util
const util = require('gulp-util');

//importando as gulpTasks criadas no modulo (arquivo) deps.js
require('./gulpTasks/app');
require('./gulpTasks/deps');
require('./gulpTasks/server');

//declaração da task padrão sempre que for executado o comando gulp no console
gulp.task('default', function () {
   gulp.start('deps', 'app', 'server');
});